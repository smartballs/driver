// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Configuration specific to the DEBUG UI interface

package config

import "gitlab.com/smartballs/driver/pkg/cfg"

var Ui = struct {
	ApplicationTitle  string
	ScreenWidth       int // in pixels
	ScreenHeight      int // in pixels
	ScreenScale       float64
	DelayBetweenInput int // in nanoseconds
}{
	ApplicationTitle:  cfg.Str("APPLICATION_TITLE").ByDefault("Smartball driver"),
	ScreenWidth:       cfg.Int("SCREEN_WIDTH").ByDefault(640),
	ScreenHeight:      cfg.Int("SCREEN_HEIGHT").ByDefault(480),
	ScreenScale:       cfg.Float64("SCREEN_SCALE").ByDefault(1),
	DelayBetweenInput: cfg.Int("DELAY_BETWEEN_INPUT").ByDefault(200000000),
}

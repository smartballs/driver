// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Configuration specific to the driver/network behavior.

package config

import "gitlab.com/smartballs/driver/pkg/cfg"

var Driver = struct {
	SmartballsAddr            string // e.g. in Ip/udp4: 239.0.0.50:8000
	DriverAddr                string
	DriverMode                string // 'udp'
	MaxSizeSmartballInputData int    // In bytes
	EnableMoregColorDebug     bool   // Send color to the smartball when a movement is recognized
}{
	// Default multicast address are given by the SB's firmware (https://github.com/siteswapjuggler/smartball-firmware)
	SmartballsAddr: cfg.Str("SMARTBALL_ADDR").ByDefault("239.0.0.50:8000"),
	DriverAddr:     cfg.Str("DRIVER_ADDR").ByDefault("239.0.0.51:9000"),

	DriverMode:                cfg.Str("DRIVER_MODE").ByDefault("udp"),
	MaxSizeSmartballInputData: cfg.Int("MAX_SIZE_SMARTBALL_INPUT_DATA").ByDefault(64),
	EnableMoregColorDebug:     cfg.Boolean("ENABLE_MOREG_COLOR_DEBUG").ByDefault(false),
}

// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

package main

import (
	"gitlab.com/smartballs/driver/internal/driver"
	"gitlab.com/smartballs/driver/internal/ui"
)

func main() {

	chanelInput := make(chan interface{})
	chanelOutput := make(chan interface{})

	go driver.Start(chanelInput, chanelOutput)

	ui.Start(chanelInput, chanelOutput)
}

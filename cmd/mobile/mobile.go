// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

package mobile

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/mobile"
	"gitlab.com/smartballs/driver/config"
	"gitlab.com/smartballs/driver/internal/driver"
	"gitlab.com/smartballs/driver/internal/ui"
	"log"
)

type userInterface struct {
	bind *ui.UI
}

func (b *userInterface) Update(screen *ebiten.Image) error {
	return b.bind.Loop(screen)
}

func (g *userInterface) Layout(viewWidth, viewHeight int) (screenWidth, screenHeight int) {
	return config.Ui.ScreenWidth, config.Ui.ScreenHeight
}

func init() {

	chanelInput := make(chan interface{})
	chanelOutput := make(chan interface{})

	go driver.Start(chanelInput, chanelOutput)

	b, err := ui.NewUI(chanelInput, chanelOutput)

	if err != nil {
		log.Fatal(err)
	}

	mobile.SetGame(&userInterface{bind: b})
}

// Dummy is a dummy exported function.
//
// gomobile doesn't compile a package that doesn't include any exported function.
// Dummy forces gomobile to compile this package.
func Dummy() {}

package com.gitlab.smartball.driver;

import android.content.Context;
import android.util.AttributeSet;

import com.crashlytics.android.Crashlytics;
import com.gitlab.smartball.driver.mobile.EbitenView;

class EbitenViewWithErrorHandling extends EbitenView {
    public EbitenViewWithErrorHandling(Context context) {
        super(context);
    }

    public EbitenViewWithErrorHandling(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected void onErrorOnGameUpdate(Exception e) {
        // You can define your own error handling e.g., using Crashlytics.
        Crashlytics.logException(e);
    }
}

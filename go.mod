module gitlab.com/smartballs/driver

go 1.13

require (
	github.com/Tarliton/collision2d v0.0.0-20160527013055-f7a088279920
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/hajimehoshi/ebiten v1.10.0-alpha.4
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/image v0.0.0-20190829233526-b3c06291d021
)

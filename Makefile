# @Author Maxime Lambert - maxime.lambert.14@outlook.fr

ANDROID_GOPATH= ${GOPATH}/src/gitlab.com/smartballs/driver

all: dep build run

clean:
	rm -rf $(ANDROID_GOPATH)
dep:
	go mod download
build:
	GOOS=linux GO111MODULE=on go build gitlab.com/smartballs/driver/cmd/desktop
run:
	go run gitlab.com/smartballs/driver/cmd/desktop
test:
	go test gitlab.com/smartballs/driver/... -v -coverprofile .coverage.txt
	go tool cover -func .coverage.txt
coverage: test
	go tool cover -html=.coverage.txt
android:
	rm -rf $(ANDROID_GOPATH)
	mkdir -p $(ANDROID_GOPATH)
	cp -r ./* $(ANDROID_GOPATH)
	cd $(ANDROID_GOPATH)
	env GO111MODULE=off ebitenmobile bind -target android -javapkg com.gitlab.smartball.driver -o ./cmd/mobile/android/ebiten/ebiten.aar gitlab.com/smartballs/driver/cmd/mobile
// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

package smartball

// Number of expected colors to be in a specific foreground color mode. @see documentation -> COLOUR (FOREGROUND)
const FULL_COLOR_MODE = 1
const HEMISPHERE_COLOR_MODE = 2
const OPPOSITE_PAIRS_COLOR_MODE = 3
const INDIVIDUAL_COLOR_MODE = 6

type Color struct {
	R byte
	G byte
	B byte
}

func RedColor() Color {
	return Color{R: 255}
}

func GreenColor() Color {
	return Color{G: 255}
}

func YellowColor() Color {
	return Color{R: 255, G: 255}
}

func CianColor() Color {
	return Color{G: 255, B: 255}
}

func PurpleColor() Color {
	return Color{R: 255, B: 255}
}

func WhiteColor() Color {
	return Color{R: 255, G: 255, B: 255}
}

func NoColor() Color {
	return Color{}
}

func OrangeColor() Color {
	return Color{R: 255, G: 153, B: 51}
}

// Flatten the RGB components from the given color slice.
// e.g. [{r1,g1,b1}, {r2,g2,b2}] -> [r1,g1,b1,r2,g2,b2]
func FlattenColorSlice(colors []Color) []byte {
	output := []byte{}

	for _, c := range colors {
		output = append(output, c.R, c.G, c.B)
	}

	return output
}

// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

package smartball

import "gitlab.com/smartballs/driver/pkg/bit"

type ImuData struct {
	Serial uint16 // Smartball emitter serial

	Flag byte

	Acc Xyz
	Gyr Xyz
	Mag Xyz

	Tmp float64

	AccN float64
	GyrN float64
	MagN float64

	Qua Quaternion

	Wld Xyz

	sta byte
}

func SetVecInFlag(flag byte) byte {
	return bit.Set(flag, bit.F4)
}

func SetQuaInFlag(flag byte) byte {
	return bit.Set(flag, bit.F5)
}

func SetWldInFlag(flag byte) byte {
	return bit.Set(flag, bit.F6)
}

type Xyz struct {
	X float64
	Y float64
	Z float64
}

type Quaternion struct {
	W float64
	X float64
	Y float64
	Z float64
}

func (d *ImuData) IsAcc() bool {
	return bit.Has(d.Flag, bit.F0)
}

func (d *ImuData) IsGyr() bool {
	return bit.Has(d.Flag, bit.F1)
}

func (d *ImuData) IsMag() bool {
	return bit.Has(d.Flag, bit.F2)
}

func (d *ImuData) IsTmp() bool {
	return bit.Has(d.Flag, bit.F3)
}

func (d *ImuData) IsVec() bool {
	return bit.Has(d.Flag, bit.F4)
}

func (d *ImuData) IsQua() bool {
	return bit.Has(d.Flag, bit.F5)
}

func (d *ImuData) IsWld() bool {
	return bit.Has(d.Flag, bit.F6)
}

func (d *ImuData) IsSta() bool {
	return bit.Has(d.Flag, bit.F7)
}

package smartball_test

import (
	"gitlab.com/smartballs/driver/pkg/smartball"
	"math"
	"testing"
)

func TestFromFixed(t *testing.T) {

	type expect struct {
		val float64
		err bool
	}

	type dataItems struct {
		data     []byte // input
		n        int    // input
		expected expect
	}

	data := []dataItems{
		{nil, 0, expect{math.MaxFloat64, true}},
		{nil, 1, expect{math.MaxFloat64, true}},
		{nil, -1, expect{math.MaxFloat64, true}},

		{[]byte{}, 0, expect{math.MaxFloat64, true}},
		{[]byte{}, 1, expect{math.MaxFloat64, true}},
		{[]byte{}, -1, expect{math.MaxFloat64, true}},

		{[]byte{255, 214, 230}, 2, expect{math.MaxFloat64, true}},
		{[]byte{255, 214, 230, 231, 232}, 2, expect{math.MaxFloat64, true}},
		{[]byte{255, 214, 230, 231, 232, 234}, 2, expect{math.MaxFloat64, true}},
		{[]byte{255, 214, 230, 231, 232, 234, 235}, 2, expect{math.MaxFloat64, true}},

		{[]byte{255, 214}, 0, expect{-42, false}},
		{[]byte{255, 214}, 1, expect{-4.2, false}},
		{[]byte{255, 214}, 2, expect{-0.42, false}},
		{[]byte{255, 214}, -1, expect{-420, false}},

		{[]byte{2, 195}, 0, expect{707, false}},
		{[]byte{2, 195}, 1, expect{70.7, false}},
		{[]byte{2, 195}, 2, expect{7.07, false}},
		{[]byte{2, 195}, -1, expect{7070, false}},

		{[]byte{2}, 0, expect{2, false}},
		{[]byte{2}, 1, expect{0.2, false}},

		{[]byte{0, 1, 0, 0}, 0, expect{65536, false}},
		{[]byte{255, 255, 255, 255}, 1, expect{-0.1, false}},

		{[]byte{0, 0, 0, 0, 0, 0, 0, 3}, 0, expect{3, false}},
		{[]byte{0, 0, 0, 0, 0, 0, 0, 3}, 1, expect{0.3, false}},
		{[]byte{1, 0, 0, 0, 0, 0, 0, 0}, 0, expect{72057594037927936, false}},
		{[]byte{255, 255, 255, 255, 255, 255, 255, 254}, 1, expect{-0.2, false}},
	}

	for _, item := range data {
		r, e := smartball.FromFixed(item.data, item.n)

		if e == nil && item.expected.err {
			t.Errorf("error expected")
		}

		if item.expected.val != r {
			t.Errorf("expected: %f, got: %f ", item.expected.val, r)
		}
	}
}

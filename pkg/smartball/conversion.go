// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

package smartball

import (
	"encoding/binary"
	"fmt"
	"gitlab.com/smartballs/driver/pkg/bit"
	"math"
)

// Converts a fixed point (not form Q format but from a proprietary protocol) to a float64
// data: raw data. CPLT2 will be considered
// n: Number / 10 ^n. i.e. Q14.2 = Number / 10^2
func FromFixed(data []byte, n int) (float64, error) {

	var msbOffset int // Relative to a uint64
	dL := len(data)

	switch {
	case data == nil, len(data) == 0:
		return math.MaxFloat64, fmt.Errorf("Cannot convert a nil or empty fixed point data to float64 ")
	case dL == 1, dL == 2, dL == 4, dL == 8: // uint8, uint16, uint32, uint64
		msbOffset = dL - 1 // from length to index
	default:
		return math.MaxFloat64, fmt.Errorf("Cannot convert the given fixed point data to float64. Bytes len: %d expected: 1, 2, 4, 8 ", len(data))
	}

	raw := convert(data)

	var signed float64

	if bit.Has(data[0], bit.F7) { // negative number, apply  CPLT2
		inverted := (raw ^ mask(msbOffset)) + 1
		signed = -1 * float64(inverted)
	} else {
		signed = float64(raw)
	}

	return signed / math.Pow10(n), nil // Apply final "home made" transformation
}

// Converts to uint64 by adding leading zeros if needed
func convert(data []byte) uint64 {

	const sizeUint64 = 8 // in bytes

	if len(data) == 8 {
		return binary.BigEndian.Uint64(data)
	}

	missingBytes := sizeUint64 - len(data)

	var leadingZeros []byte
	for i := 0; i < missingBytes; i++ {
		leadingZeros = append([]byte{0}, leadingZeros...)
	}

	return binary.BigEndian.Uint64(append(leadingZeros, data...))
}

// Returns a mask full of one. Useful to apply a xor.
// offset: index of byte to be filled with 1
func mask(offset int) uint64 {

	fromOffsetToBits := (offset + 1) * 8 // from index to number. Times byte in bits
	out := uint64(0)

	for i := 0; i < fromOffsetToBits; i++ {
		out += 1 << i
	}

	return out
}

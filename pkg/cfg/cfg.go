// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Allows to easily configure environment variables

package cfg

import (
	"fmt"
	"os"
	"strconv"
)

// Environment string config param

type Str string

func (env Str) ByDefault(def string) string {

	s, b := os.LookupEnv(string(env))

	if b {
		return s
	} else {
		return def
	}
}

func (env Str) WithoutDefault() string {

	s, b := os.LookupEnv(string(env))

	if !b {
		_, _ = fmt.Fprintf(os.Stderr, "Missing string environment variable: %s", env)
		os.Exit(1)
	}

	return s
}

// Environment Int config param

type Int string

func (env Int) ByDefault(def int) int {
	envInt, b := os.LookupEnv(string(env))

	i, e := strconv.ParseInt(envInt, 10, 64)
	if b && e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to integer value was: '%s'", env, envInt)
		os.Exit(1)
	}

	if !b {
		return def
	} else {
		return int(i)
	}
}

func (env Int) WithoutDefault() int {
	envInt, b := os.LookupEnv(string(env))

	if !b {
		_, _ = fmt.Fprintf(os.Stderr, "Missing integer environment variable: %s", env)
		os.Exit(1)
	}

	i, e := strconv.ParseInt(envInt, 10, 64)
	if e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to integer value was: '%s'", env, envInt)
		os.Exit(1)
	}

	return int(i)
}

// Environment numerical config param

type Float64 string

func (env Float64) ByDefault(def float64) float64 {
	envFloat, b := os.LookupEnv(string(env))

	f, e := strconv.ParseFloat(envFloat, 64)
	if b && e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to float64 value was: '%s'", env, envFloat)
		os.Exit(1)
	}

	if !b {
		return def
	} else {
		return f
	}
}

func (env Float64) WithoutDefault() float64 {
	envFloat, b := os.LookupEnv(string(env))

	if !b {
		_, _ = fmt.Fprintf(os.Stderr, "Missing float64 environment variable: %s", env)
		os.Exit(1)
	}

	f, e := strconv.ParseFloat(envFloat, 64)
	if e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to float64 value was: '%s'", env, envFloat)
		os.Exit(1)
	}

	return f
}

// Environment Boolean config param

type Boolean string

func (env Boolean) ByDefault(def bool) bool {
	envBool, present := os.LookupEnv(string(env))

	b, e := strconv.ParseBool(envBool)
	if present && e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to bool value was: '%s'", env, envBool)
		os.Exit(1)
	}

	if !present {
		return def
	} else {
		return bool(b)
	}
}

func (env Boolean) WithoutDefault() bool {
	envBool, present := os.LookupEnv(string(env))

	if !present {
		_, _ = fmt.Fprintf(os.Stderr, "Missing Boolean environment variable: %s", env)
		os.Exit(1)
	}

	b, e := strconv.ParseBool(envBool)
	if e != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to convert environment variable '%s' to bool value was: '%s'", env, envBool)
		os.Exit(1)
	}

	return bool(b)
}

// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

package ui

import (
	"github.com/Tarliton/collision2d"
	"github.com/hajimehoshi/ebiten"
	"gitlab.com/smartballs/driver/config"
	"time"
)

type Input struct {
	IsClicked       bool
	LatestClickTime int64
	LatestClickPos  collision2d.Vector
}

func (i *Input) GetMousePos() collision2d.Vector {

	x, y := ebiten.CursorPosition()

	return collision2d.Vector{
		X: float64(x),
		Y: float64(y),
	}
}

func (i *Input) Update() {

	i.IsClicked = false

	if !i.isDelayOk() {
		return
	}

	// Emulates the keys by touching
	if isMobile() {
		x, y := ebiten.TouchPosition(0)
		if x != 0 && y != 0 {
			i.LatestClickPos = collision2d.Vector{
				X: float64(x),
				Y: float64(y),
			}
			i.IsClicked = true
		}
	} else {
		if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
			x, y := ebiten.CursorPosition()
			i.LatestClickPos = collision2d.Vector{
				X: float64(x),
				Y: float64(y),
			}
			i.IsClicked = true
		}
	}

	i.LatestClickTime = time.Now().UnixNano()
}

func (i *Input) isDelayOk() bool {
	return time.Now().UnixNano()-i.LatestClickTime > int64(config.Ui.DelayBetweenInput)
}

func isMobile() bool {
	return ebiten.TouchIDs() != nil
}

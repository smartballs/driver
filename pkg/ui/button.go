// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

package ui

import (
	"github.com/Tarliton/collision2d"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/text"
	"golang.org/x/image/font"
	"image/color"
)

type Button struct {
	background *ebiten.Image
	opts       *ebiten.DrawImageOptions
	hitbox     collision2d.Box
	label      string
	txtOffset  collision2d.Vector
}

func NewButton(label string, color color.NRGBA, size collision2d.Vector, pos collision2d.Vector, txtOffset collision2d.Vector) Button {

	background, _ := ebiten.NewImage(int(size.X), int(size.Y), ebiten.FilterDefault)
	_ = background.Fill(color)

	button := Button{
		background: background,
		opts:       &ebiten.DrawImageOptions{},
		hitbox:     collision2d.Box{W: size.X, H: size.Y, Pos: pos},
		txtOffset:  txtOffset,
	}

	button.opts.GeoM.Translate(pos.X, pos.Y)

	return button
}

func (b *Button) Draw(screen *ebiten.Image, font font.Face) {
	_ = screen.DrawImage(b.background, b.opts)

	text.Draw(screen, b.label, font, int(b.hitbox.Pos.X+b.txtOffset.X), int(b.hitbox.Pos.Y+b.txtOffset.Y), color.White)
}

func (b *Button) IsClicked(input *Input) bool {

	if !input.IsClicked {
		return false
	}

	return collision2d.PointInPolygon(input.LatestClickPos, b.hitbox.ToPolygon())
}

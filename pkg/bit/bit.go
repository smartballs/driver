// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Simple bit operations

package bit

const (
	F0 byte = 1
	F1 byte = 1 << 1
	F2 byte = 1 << 2
	F3 byte = 1 << 3
	F4 byte = 1 << 4
	F5 byte = 1 << 5
	F6 byte = 1 << 6
	F7 byte = 1 << 7
)

func Set(b, flag byte) byte    { return b | flag }
func Clear(b, flag byte) byte  { return b &^ flag }
func Toggle(b, flag byte) byte { return b ^ flag }
func Has(b, flag byte) bool {
	return b&flag != 0
}

# @Author Maxime Lambert - maxime.lambert.14@outlook.fr

FROM golang:1.13.1 as build

WORKDIR /app/bin

COPY . .

RUN GOOS=linux GO111MODULE=on go build gitlab.com/smartballs/driver/cmd/desktop

FROM ubuntu:19.04

COPY --from=build /app/bin/desktop /app/bin/desktop

ENTRYPOINT ["/app/bin/desktop"]

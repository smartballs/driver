# Smartball project - Moreg Driver

This repository contains an UDP driver to communicate with Smartballs and perform client-side motion recognition.

The purpose of this repository is to demonstrate that other uses are possible outside of juggling. The idea is to make Smartball a gaming controller to hit a wider audience.

## The Smartball project

The Smartball project is an Open Source connected juggling ball project. 

It was initiated by [Mr. Sylvain Garnavault](https://github.com/siteswapjuggler) in 2018.

## Getting started

This project is designed to be cross-platform (Windows, Linux, Mac OS X, and Android), with a 95% common code base. 

### Prerequisites

Before you begin you will need Go v1.13 at least.

If you plan to compile on mobile platform (Android), you must have a fully functional Android SDK installed.
Then follows the instructions here on [Ebiten website](https://ebiten.org/documents/mobile.html).

### Hello Smartball

* Make sure the SB is on the same network.
* If you're on Windows, please paid attention to your firewall. It may blocks incoming UDP datagrams from the SB.

```
  make dep  # Download go module dependencies.
  make run
```

## Limitations

The motion recognition is done on client-side. IMU data passes through the network at 100 Hz.
Moreover, the imu chip cost about 3€, we're facing challenges to have a reliable motion recognition with an acceptable latency.

A debug window is available to monitor smartball IMU data. For the moment no distinction is made between IMU data from one or more smartballs.
That is why it is recommended to connect only one smartball if you want to trace IMU data.

Outside this debugging window, motion recognition and command sending fully support multiple smartballs.

## Environment variables

The following environment variables allows to configure the driver's behavior.

| Name | Description | Default value |
|------|-------------|---------------|
|__DRIVER__|||
| SMARTBALL_ADDR | Address of the smartball(s). If driver mode is set to `udp`, it must be `IP_V4:PORT`. It could be a unicast address to drive only 1 smartball or a multicast address. | 239.0.0.50:8000 |
| DRIVER_ADDR | Address of the driver. If driver mode is set to `udp`, it must be `IP_V4:PORT`. It could be a unicast or a multicast address | 239.0.0.51:9000 |
| DRIVER_MODE | Mode of the driver. Available: `udp`. Susceptible to be extended in the futur.| udp|
| MAX_SIZE_SMARTBALL_INPUT_DATA | Maximum size of the data sent by the smartball. In bytes| 16|
| ENABLE_MOREG_COLOR_DEBUG | Set a specific color to the Smartball when a movement is recognized. | false |

## Testing

Because of the nature of the Smartball unit testing does not have much meaning apart from the serialization/deserialization of frames.

A suite of tools designed by [Guillaume Picard](gpicarg@ecole.ensicaen.fr) and [Luigi Dechanteloup](ldechanteloup@ecole.ensicaen.fr) allows to analyse and replay smartball UDP datagrams.

Our goal is to perform integration tests by simulating a smarball via a test set captured from wireshark and then manually stripped. Then, we will compare what the driver has detected with what was expected.

## Roadmap

* Enable compilation and run driver on all targeted platforms __[DONE]__
* Provide a debug interface __[DONE]__
* Basic POC regarding motion recognition __[DONE]__
* Stable udp driver __[DONE]__
* Auto-configure smartball to enable motion recognition __[DONE]__
* Export movement events to a messaging broker (e.g. ZMQ)
* Perform integration testing thanks to the tool [replay packets](https://gitlab.com/smartballs/replay-packets)
* Add the integration stage inside the pipeline
* Expose KPI regarding motion recognition
* Enhance motion recognition
* Integrate the video game developed by [Adbelmalik Ghoubir](ghoubir@ecole.ensicaen.fr)

## License

All the software and documentations are under open source licenses [GPL v3](http://www.gnu.org/licenses/gpl-3.0.html).

## Linked repositories

This repository refers directly to its following siblings :

* [https://github.com/siteswapjuggler/smartball-imu]()
* [https://github.com/siteswapjuggler/smartball-firmware]()
* [https://github.com/siteswapjuggler/smartball-externals]()
* [https://github.com/siteswapjuggler/smartball-hardware]()
* [https://github.com/siteswapjuggler/smartball-documents]()

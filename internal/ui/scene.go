// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Debug UI to visualize IMU data and send basic command

package ui

import (
	"fmt"
	"github.com/Tarliton/collision2d"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/text"
	"gitlab.com/smartballs/driver/pkg/bit"
	"gitlab.com/smartballs/driver/pkg/smartball"
	"gitlab.com/smartballs/driver/pkg/ui"
	"image/color"
)

type Scene interface {
	Update(g *UI)
	Draw(screen *ebiten.Image, ui *UI)
}

// TODO: remove dependencies to udp package
type IMUScene struct {
	data smartball.ImuData

	current string
	color   color.NRGBA // Current smartball color, TODO: receive color from datagram instead
	motor   [2]byte
	flag    byte

	colorButton ui.Button
	motorButton ui.Button
	flagButton  ui.Button
}

func newIMUScene(chanelInput chan interface{}) Scene {

	red := color.NRGBA{0xFF, 0x00, 0x00, 0xFF}

	s := &IMUScene{
		colorButton: ui.NewButton("Color",
			red,
			collision2d.Vector{150, 30},
			collision2d.Vector{10, 400},
			collision2d.Vector{27, 33}),
		motorButton: ui.NewButton("Motor",
			red,
			collision2d.Vector{150, 30},
			collision2d.Vector{150, 400},
			collision2d.Vector{27, 33}),
		flagButton: ui.NewButton("Flag",
			red,
			collision2d.Vector{150, 30},
			collision2d.Vector{290, 400},
			collision2d.Vector{27, 33}),
		color:   color.NRGBA{0x00, 0x00, 0x00, 0xFF},
		motor:   [2]byte{0, 0},
		flag:    0x01,
		current: "none",
	}

	go func(chanel chan interface{}, scene *IMUScene) {
		for {
			payload := <-chanel

			data, ok := payload.(smartball.ImuData)
			if !ok {
				return
			}

			scene.data = data
		}
	}(chanelInput, s)

	return s
}

func (t *IMUScene) changeFlag() {
	switch {
	case bit.Has(t.flag, bit.F7):
		t.flag = 0x01
	case bit.Has(t.flag, bit.F6):
		t.flag = bit.Set(t.flag, bit.F7)
	case bit.Has(t.flag, bit.F5):
		t.flag = bit.Set(t.flag, bit.F6)
	case bit.Has(t.flag, bit.F4):
		t.flag = bit.Set(t.flag, bit.F5)
	case bit.Has(t.flag, bit.F3):
		t.flag = bit.Set(t.flag, bit.F4)
	case bit.Has(t.flag, bit.F2):
		t.flag = bit.Set(t.flag, bit.F3)
	case bit.Has(t.flag, bit.F1):
		t.flag = bit.Set(t.flag, bit.F2)
	case bit.Has(t.flag, bit.F0):
		t.flag = bit.Set(t.flag, bit.F1)
	}
}

func (t *IMUScene) changeMotor() {
	if t.motor[0] == 0 {
		t.motor = [2]byte{0xFF, 0xFF}
	} else {
		t.motor = [2]byte{0x00, 0x00}
	}
}

func (t *IMUScene) changeSmartballColor() {

	switch t.current {
	case "none":
		t.color = color.NRGBA{0xFF, 0x00, 0x00, 0xFF}
		t.current = "red"
	case "red":
		t.color = color.NRGBA{0x00, 0xFF, 0x00, 0xFF}
		t.current = "green"
	case "green":
		t.color = color.NRGBA{0x00, 0x00, 0xFF, 0xFF}
		t.current = "blue"
	case "blue":
		t.color = color.NRGBA{0x00, 0x00, 0x00, 0xFF}
		t.current = "none"
	}
}

func (t *IMUScene) Update(ui *UI) {

	go func() {
		if t.colorButton.IsClicked(&ui.input) {
			t.changeSmartballColor()
			ui.chanelOutput <- []byte{0x20, t.color.R, t.color.G, t.color.B}
		}

		if t.motorButton.IsClicked(&ui.input) {
			t.changeMotor()
			ui.chanelOutput <- []byte{0x50, t.motor[1], t.motor[0]}
		}

		if t.flagButton.IsClicked(&ui.input) {
			t.changeFlag()
			ui.chanelOutput <- []byte{0x30, t.flag}
		}
	}()
}

func (t *IMUScene) Draw(screen *ebiten.Image, ui *UI) {

	const mediumOffset = 25

	screen.Size()

	// Buttons

	t.flagButton.Draw(screen, ui.font.regular)
	//t.motorButton.Draw(screen, ui.font.regular)
	//t.colorButton.Draw(screen, ui.font.regular)

	// IMU

	if t.data.IsAcc() {
		text.Draw(screen, fmt.Sprintf("Acc | X: %+07.2f     Y: %+07.2f     Z: %+07.2f", t.data.Acc.X, t.data.Acc.Y, t.data.Acc.Z), ui.font.regular, 10, mediumOffset, color.White)
	}

	if t.data.IsGyr() {
		text.Draw(screen, fmt.Sprintf("Gyr | X: %+07.2f     Y: %+07.2f     Z: %+07.2f", t.data.Gyr.X, t.data.Gyr.Y, t.data.Gyr.Z), ui.font.regular, 10, 3*mediumOffset, color.White)
	}

	if t.data.IsMag() {
		text.Draw(screen, fmt.Sprintf("Mag | X: %+07.2f     Y: %+07.2f     Z: %+07.2f", t.data.Mag.X, t.data.Mag.Y, t.data.Mag.Z), ui.font.regular, 10, 5*mediumOffset, color.White)
	}

	if t.data.IsTmp() {
		text.Draw(screen, fmt.Sprintf("Tmp | T: %+07.2f", t.data.Tmp), ui.font.regular, 10, 7*mediumOffset, color.White)
	}

	if t.data.IsVec() {
		text.Draw(screen, fmt.Sprintf("Vec | A: %+07.2f     G: %+07.2f     M: %+07.2f", t.data.AccN, t.data.GyrN, t.data.MagN), ui.font.regular, 10, 9*mediumOffset, color.White)
	}

	if t.data.IsQua() {
		text.Draw(screen, fmt.Sprintf("Qua | X: %+07.2f     Y: %+07.2f     Z: %+07.2f     W: %+07.2f", t.data.Qua.X, t.data.Qua.Y, t.data.Qua.Z, t.data.Qua.W), ui.font.regular, 10, 11*mediumOffset, color.White)
	}

	if t.data.IsWld() {
		text.Draw(screen, fmt.Sprintf("Wld | X: %+07.2f     Y: %+07.2f     Z: %+07.2f", t.data.Wld.X, t.data.Wld.Y, t.data.Wld.Z), ui.font.regular, 10, 13*mediumOffset, color.White)
	}
}

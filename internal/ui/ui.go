// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Bind debug ui with ebiten framework

package ui

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	log "github.com/sirupsen/logrus"
	"gitlab.com/smartballs/driver/config"
	"gitlab.com/smartballs/driver/pkg/ui"
)

type UI struct {
	chanelInput  chan interface{}
	chanelOutput chan interface{}

	scene Scene
	font  fontRepository
	input ui.Input
}

func Start(chanelInput chan interface{}, chanelOutput chan interface{}) {

	debugUi, err := NewUI(chanelInput, chanelOutput)

	if err != nil {
		log.Fatal(err)
	}

	err = ebiten.Run(debugUi.Loop,
		config.Ui.ScreenWidth,
		config.Ui.ScreenHeight,
		config.Ui.ScreenScale,
		config.Ui.ApplicationTitle)

	if err != nil {
		log.Fatal(err)
	}

}

func NewUI(chanelInput chan interface{}, chanelOutput chan interface{}) (*UI, error) {
	return &UI{
		font:         newFontRepository(),
		scene:        newIMUScene(chanelInput),
		chanelInput:  chanelInput,
		chanelOutput: chanelOutput,
		input:        ui.Input{},
	}, nil
}

func (ui *UI) Loop(screen *ebiten.Image) error {

	if ui.scene == nil {
		return fmt.Errorf("Scene cannot be null ")
	}

	ui.input.Update()

	ui.scene.Update(ui)

	ui.scene.Draw(screen, ui)

	return nil
}

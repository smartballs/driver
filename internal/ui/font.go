// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Fonts of the debug UI

package ui

import (
	"github.com/golang/freetype/truetype"
	log "github.com/sirupsen/logrus"
	"gitlab.com/smartballs/driver/assets/fonts"
	"golang.org/x/image/font"
)

type fontRepository struct {
	regular font.Face
}

func newFontRepository() fontRepository {
	tt, err := truetype.Parse(fonts.MPlus1pRegular_ttf)
	if err != nil {
		log.Fatal(err)
	}

	const dpi = 72
	return fontRepository{
		regular: truetype.NewFace(tt, &truetype.Options{
			Size:    22,
			DPI:     dpi,
			Hinting: font.HintingFull,
		}),
	}
}

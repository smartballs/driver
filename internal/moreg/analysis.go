// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// CAUTION - EXPERIMENTAL PACKAGE - WORK IN PROGRESS
// Raw analysis of the IMU data to infer movement

package MoReg

import log "github.com/sirupsen/logrus"

func (tracker *Tracker) analyse() {

	if len(tracker.data) <= 0 {
		tracker.state = StateIdle
		return
	}

	latest := tracker.data[len(tracker.data)-1]

	if latest.Wld.Z >= 8.5 && latest.Wld.Z <= 12 && latest.GyrN <= 5 {
		tracker.state = StateIdle
		tracker.eventChannel <- StateIdle
		return
	}

	tracker.checkThrow()

	tracker.checkDirectFreeFall()

}

func (tracker *Tracker) checkDirectFreeFall() {
	if tracker.state == StateUp || tracker.state == StateDown || len(tracker.data) < 4 {
		return
	}

	if tracker.data[len(tracker.data)-4].Wld.Z > tracker.data[len(tracker.data)-3].Wld.Z*1.25 &&
		tracker.data[len(tracker.data)-3].Wld.Z > tracker.data[len(tracker.data)-2].Wld.Z*1.25 &&
		tracker.data[len(tracker.data)-2].Wld.Z > tracker.data[len(tracker.data)-1].Wld.Z*1.25 &&
		tracker.data[len(tracker.data)-4].GyrN <= tracker.data[len(tracker.data)-3].GyrN*1.1 &&
		tracker.data[len(tracker.data)-3].GyrN <= tracker.data[len(tracker.data)-2].GyrN*1.1 &&
		tracker.data[len(tracker.data)-2].GyrN <= tracker.data[len(tracker.data)-1].GyrN*1.1 &&
		tracker.data[len(tracker.data)-4].GyrN >= tracker.data[len(tracker.data)-3].GyrN*0.9 &&
		tracker.data[len(tracker.data)-3].GyrN >= tracker.data[len(tracker.data)-2].GyrN*0.9 &&
		tracker.data[len(tracker.data)-2].GyrN >= tracker.data[len(tracker.data)-1].GyrN*0.9 {
		tracker.state = StateRoll
		tracker.eventChannel <- StateRoll
	}
}

func (throw *throw) reset() {
	throw.start = nil
	throw.max = nil
	throw.detected = false
	throw.frames = 0
	throw.delay = 0
	throw.peakIndex = 0
}

func (tracker *Tracker) checkThrow() {

	latest := &tracker.data[len(tracker.data)-1]

	if !tracker.throwInfos.detected {
		if tracker.throwInfos.start == nil { //Potential throw detection
			if latest.Wld.Z >= 11 {
				tracker.throwInfos.start = latest
				tracker.throwInfos.frames = 0
				return
			}
		} else if tracker.throwInfos.max == nil { //Potential max throw acc
			tracker.throwInfos.max = latest
			tracker.throwInfos.frames = 1
			return
		} else {
			tracker.throwInfos.frames++

			if latest.Wld.Z >= tracker.throwInfos.max.Wld.Z { //Potential max throw acc
				tracker.throwInfos.max = latest
				tracker.throwInfos.peakIndex = 0
			} else { //Throw detected at index - 1
				tracker.throwInfos.peakIndex++
				if tracker.throwInfos.peakIndex >= 3 {
					tracker.throwInfos.reset()
					return
				}
				if tracker.throwInfos.max.Wld.Z <= tracker.throwInfos.start.Wld.Z*1.5 || tracker.throwInfos.frames < 5 {
					// False positive reject
					tracker.throwInfos.reset()
					return
				}
				tracker.throwInfos.delay = computePeak(tracker.throwInfos.start.Wld.Z, tracker.throwInfos.max.Wld.Z, tracker.throwInfos.frames*10)
				if tracker.throwInfos.delay <= 150 {
					tracker.throwInfos.reset()
					return
				}
				log.Info("Delay :", tracker.throwInfos.delay)

				tracker.throwInfos.detected = true
				tracker.state = StateUp
				tracker.eventChannel <- StateUp
			}
		}
	} else {
		tracker.throwInfos.delay -= 10
		if tracker.throwInfos.delay <= 0 {
			tracker.state = StateDown
			tracker.eventChannel <- StateDown
			//Tracker reset
			tracker.throwInfos.reset()
		}
	}
}

func computePeak(min float64, max float64, time int) int {
	va := (max - min) * (float64(time) / 1000)
	peak := va / 9.81
	return int(peak * 1000) //+-30 ms
}

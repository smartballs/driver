// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// CAUTION - EXPERIMENTAL PACKAGE - WORK IN PROGRESS
// Motino recognition manager

package MoReg

import (
	"fmt"
	sb "gitlab.com/smartballs/driver/pkg/smartball"
)

// TODO: Find a way to return the "velocity" of a movement + infer the deceleration (help to predict the end of a movement by the client).

type State int

// TODO: Consider switch to a struct with boolean (isRoll, isUp....) Much more convenient to interact with than a slice
// A Smartball could have a combination of states. e.g. StateRoll + StateUp
type States []State

const (
	StateUnknown = -1
	StateIdle    = 0
	StateUp      = 1
	StateDown    = 2
	StateRoll    = 3
	StateMouving = 4
)

const CURRENT_STATE = 0
const AVAILABLE_STATE_AMOUNT = 5

// An event denotes a transition between one state to another
type Event int

const (
	// Generic state transition e.g. the ball is stopping rolling on a table
	EventStateTransition = 0
	// e.g. The ball shocked on the floor
	EventShock = 1
	// Specific transition between StateUp and StateDown
	EventApogee = 2
)

const AVAILABLE_IMU_DATA_AMOUNT = 20 // At 100Hz, 20 samples is equivalent to 200 ms buffer

type MoregManager interface {
	// Take account an imu Data input
	Process(data sb.ImuData) error

	GetCurrentState() State

	GetRequiredFlagSetup() byte
}

type throw struct {
	start     *sb.ImuData
	max       *sb.ImuData
	frames    int
	detected  bool
	delay     int
	peakIndex int
}

// Allows to track states of a smartball. Offers motion recognition (states & events)
type Tracker struct {
	// The tracker will send states transitions (event) through this channel
	eventChannel chan Event

	data       []sb.ImuData // Used as a FIFO with a capacity of 'AVAILABLE_IMU_DATA_AMOUNT'
	state      State        // See it as a FiFo, head at index 0
	throwInfos throw        // Keep infos about starting throw point and maximum throw point
}

func NewTracker(eventChannel chan Event) *Tracker {
	return &Tracker{
		eventChannel: eventChannel,
	}
}

func (tracker *Tracker) Process(data sb.ImuData) error {

	// Vec holds GyrN
	if !data.IsVec() || !data.IsWld() {
		return fmt.Errorf("Vec and Wld data must be provided to enable Motion Recognition")
	}

	// Add normalized point in internal FIFO
	tracker.buffering(data)

	tracker.analyse()

	return nil
}

// Returns the minimal flag settings required to run MoReg
func (tracker *Tracker) GetRequiredFlagSetup() byte {
	var flag byte = 0

	flag = sb.SetWldInFlag(flag)
	flag = sb.SetQuaInFlag(flag)
	flag = sb.SetVecInFlag(flag)

	return flag
}

func (tracker *Tracker) GetCurrentState() State {
	return tracker.state
}

func (tracker *Tracker) buffering(data sb.ImuData) {
	if len(tracker.data) >= AVAILABLE_IMU_DATA_AMOUNT {
		// Removes the head
		tracker.data = tracker.data[1:]
	}
	tracker.data = append(tracker.data, data)
}

// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Driver at high level view.

package driver

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/smartballs/driver/config"
	MoReg "gitlab.com/smartballs/driver/internal/moreg"
	"gitlab.com/smartballs/driver/pkg/smartball"
	"math"
)

type driver interface {
	// Opens the drivers to one or several smartballs
	open() error

	// Closes the driver
	close() error

	// Writes data to smartball(s). The provided bytes array will be written.
	write(data []byte) error

	// Reads data from smartball(s). Feeds the provided bytes array.
	read(data []byte) error

	// Tries to unmarshal imu data from provided raw bytes.
	unmarshalIMUData(data []byte) (smartball.ImuData, error)

	// Reboot the Smartball
	reboot() error

	// Set the SB's foreground color. Accept 1 [full],2 [hemisphere], 3 [opposite pairs], or 6 [individual] colors
	SetForegroundColor(...smartball.Color) error

	// Change current IMU settings
	SetIMUSettings(flag byte) error
}

type driverManager struct {
	eventChannel chan MoReg.Event
	trackers     map[uint16]MoReg.MoregManager
	delegate     driver
}

// Starts the driver which acts as a daemon.
func Start(chanelInput chan interface{}, chanelOutput chan interface{}) {
	smartballDriver, err := newDriverManager(config.Driver.DriverMode)
	if err != nil {
		log.Fatal(err)
	}

	err = smartballDriver.delegate.open()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err = smartballDriver.delegate.close()
		if err != nil {
			log.Error(err)
		}
	}()

	//TODO: goroutine to listen input from ZMQ

	// The following Go routine allows to send data to Smartballs while listen incoming commands.
	go func() {
		for {
			data := <-chanelOutput

			payload, ok := data.([]byte)
			if !ok {
				log.Errorf("Unable to convert data to byte array")
				continue
			}

			e := smartballDriver.delegate.write(payload)
			if e != nil {
				log.Errorf("Unable to send payload to smartball")
			}
		}
	}()

	smartballDriver.listen(chanelInput)
}

// Creates a new driver manager
func newDriverManager(t string) (*driverManager, error) {

	switch t {

	case "udp", "UDP", "ip/udp", "IP/UDP":
		return &driverManager{
			delegate:     &udpDriver{},
			eventChannel: make(chan MoReg.Event),
			trackers:     map[uint16]MoReg.MoregManager{},
		}, nil

	default:
		return nil, fmt.Errorf("'%s' is not a recognized as a driver manager", t)
	}
}

// This will reset the SB if Wld and Quaternion values seems to be bugged.
// Requires Wld and Quaternion flag enabled
func (dm *driverManager) handleWldAndQuaternionIssue(data smartball.ImuData) bool {
	if !data.IsWld() || !data.IsQua() {
		return false
	}

	const WLD_MIN_THRESHOLD = 0.02
	const QUA_MIN_THRESHOLD = 0.006

	if math.Abs(data.Wld.Z) > WLD_MIN_THRESHOLD ||
		math.Abs(data.Qua.X) > QUA_MIN_THRESHOLD ||
		math.Abs(data.Wld.X) > WLD_MIN_THRESHOLD ||
		math.Abs(data.Qua.W) > QUA_MIN_THRESHOLD {
		return false
	}

	err := dm.delegate.reboot()
	if err != nil {
		log.Error(err)
		return false
	}

	log.Info("Rebooting Smartball to fix Wld and Quaternion Issue...")

	// Delete ball tracker to avoid side effects.
	delete(dm.trackers, data.Serial)

	return true
}

// Send a color to smartball when an event a movement is recognized.
func (dm *driverManager) debugMoregWithColor(tracker MoReg.MoregManager) {
	if !config.Driver.EnableMoregColorDebug {
		return
	}

	go func() {
		for {
			state := <-dm.eventChannel

			if state == MoReg.StateDown {
				err := dm.delegate.SetForegroundColor(smartball.GreenColor())
				if err != nil {
					log.Errorf("Unable to set Green color while SB is free falling down")
				}
			}
			if state == MoReg.StateIdle {
				err := dm.delegate.SetForegroundColor(smartball.NoColor())
				if err != nil {
					log.Errorf("Unable to set No color while SB is idling")
				}
			}
			if state == MoReg.StateUp {
				err := dm.delegate.SetForegroundColor(smartball.RedColor())
				if err != nil {
					log.Errorf("Unable to set Red color while SB is free falling up")
				}
			}
			if state == MoReg.StateRoll {
				err := dm.delegate.SetForegroundColor(smartball.YellowColor())
				if err != nil {
					log.Errorf("Unable to set yellow color while SB is Rolling")
				}
			}
			if state == MoReg.StateUnknown {
				err := dm.delegate.SetForegroundColor(smartball.WhiteColor())
				if err != nil {
					log.Errorf("Unable to set Green color while SB is in a unknow state")
				}
			}
		}
	}()
}

// Listens data sent by the Smartball and handles them
func (dm *driverManager) listen(c chan interface{}) {

	data := make([]byte, config.Driver.MaxSizeSmartballInputData)
	var index uint32 = 0

	for {
		err := dm.delegate.read(data)
		if err != nil {
			log.Error(err)
			continue
		}

		imu, err := dm.delegate.unmarshalIMUData(data)
		if err != nil {
			log.Trace(err)
			continue
		}

		_, isAlreadyTracked := dm.trackers[imu.Serial]
		if !isAlreadyTracked {
			dm.trackers[imu.Serial] = MoReg.NewTracker(dm.eventChannel)

			dm.debugMoregWithColor(dm.trackers[imu.Serial])

			//TODO: Rather than overwriting flag settings, just add minimal required flag setup.
			err = dm.delegate.SetIMUSettings(dm.trackers[imu.Serial].GetRequiredFlagSetup())
			if err != nil {
				log.Errorf("Unable to set required IMU settings", err)
			}
		}

		isIssueFixed := dm.handleWldAndQuaternionIssue(imu)
		if isIssueFixed {
			continue
		}

		err = dm.trackers[imu.Serial].Process(imu)
		if err != nil {
			log.Error(err)
			err = dm.delegate.SetIMUSettings(dm.trackers[imu.Serial].GetRequiredFlagSetup())
			if err != nil {
				log.Errorf("Unable to set required IMU settings", err)
			}
			continue
		}

		// Avoid flooding the debug UI. UI refresh at 60Hz, SB emit at 100Hz.
		index += 1
		if index >= 5 {
			c <- imu
			index = 0
		}
	}
}

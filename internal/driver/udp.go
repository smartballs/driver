// @Author Maxime Lambert - maxime.lambert.14@outlook.fr
// Use of this source code is governed by a GPL v3 license.

// Specific implementation of the driver with IPv4/UDP

package driver

import (
	"encoding/binary"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/smartballs/driver/config"
	"gitlab.com/smartballs/driver/pkg/smartball"
	"net"
)

type udpDriver struct {
	output *net.UDPConn
	input  *net.UDPConn
}

// @see 'UDP_Datagrams_v0.3.docx - 02/10/2019 by S. Garnavault
// https://github.com/siteswapjuggler/smartball-documents/blob/master/UDP_Datagrams_v0.3.docx

const q14_2Quotient = 2      // 10^2 -> 100
const quaternionQuotient = 4 // 10^4 -> 10000

const sizeQ14_2 = 2               // in bytes
const sizeXYZ_Q14_2 = 6           // in bytes
const sizeQuaternionComponent = 2 // in bytes
const sizeQuaternion = 8          // in bytes

// Downstream packet constants
const IndexDSerial = 2 // bit index
const IndexDCmd = 4    // bit index
const IndexDData = 5   // bit index

// Downstream packet commands

//const CMD_D_PONG uint8 = '\x01'
//const CMD_D_BATTERY uint8 = '\x00'
const CmdDImu uint8 = '\x30'
const CmdReboot uint8 = '\x02'
const CmdForegroundColor uint8 = '\x20'
const CmdImuSettings uint8 = '\x30'

type UpstreamPacket struct {
	Command byte
	Data    []byte
}

type DownstreamPacket struct {
	Id      uint16
	Serial  uint16
	Command uint8
	Data    interface{}
}

func (d *udpDriver) SetIMUSettings(flag byte) error {
	return d.write(append([]byte{CmdImuSettings}, flag))
}

func (d *udpDriver) SetForegroundColor(colors ...smartball.Color) error {

	if len(colors) != smartball.FULL_COLOR_MODE &&
		len(colors) != smartball.HEMISPHERE_COLOR_MODE &&
		len(colors) != smartball.OPPOSITE_PAIRS_COLOR_MODE &&
		len(colors) != smartball.INDIVIDUAL_COLOR_MODE {
		return fmt.Errorf("Cannot set foreground color. %d is an invalid number of colors", len(colors))
	}

	return d.write(append([]byte{CmdForegroundColor}, smartball.FlattenColorSlice(colors)...))
}

func (d *udpDriver) reboot() error {

	return d.write([]byte{CmdReboot})
}

func (d *udpDriver) unmarshalIMUData(data []byte) (smartball.ImuData, error) {

	if data[IndexDCmd] != CmdDImu {
		return smartball.ImuData{}, fmt.Errorf("Command %d is not refering to IMU data", data[IndexDCmd])
	}

	imu := parseIMUdata(data[IndexDData:])
	imu.Serial = binary.BigEndian.Uint16(data[IndexDSerial:IndexDCmd])

	return imu, nil
}

func (d *udpDriver) open() error {

	log.Infof("Opening output UDP connection from '%s' to '%s'...", config.Driver.DriverAddr, config.Driver.SmartballsAddr)

	// Could be an unicast or multicast IP. Multicast recommended to drive several smartballs.
	smartballAddr, err := net.ResolveUDPAddr("udp", config.Driver.SmartballsAddr)
	if err != nil {
		return fmt.Errorf("unable to set smartball(s) udp address (%s provided): %s ", config.Driver.SmartballsAddr, err)
	}

	// Could be an unicast or multicast IP
	driverUDPAddr, err := net.ResolveUDPAddr("udp", config.Driver.DriverAddr)
	if err != nil {
		return fmt.Errorf("unable to set driver udp address (%s provided): %s ", config.Driver.DriverAddr, err)
	}

	d.output, err = net.DialUDP("udp", nil, smartballAddr)
	if err != nil {
		return fmt.Errorf("unable to dial input UDP: %s ", err)
	}

	isMulticast := isMulticast(config.Driver.DriverAddr)

	if isMulticast {
		d.input, err = net.ListenMulticastUDP("udp", nil, driverUDPAddr)
		if err != nil {
			return fmt.Errorf("unable to listen multicast udp (%s provided): %s ", config.Driver.DriverAddr, err)
		}
	} else {
		d.input, err = net.ListenUDP("udp", driverUDPAddr)
		if err != nil {
			return fmt.Errorf("unable to listen unicast udp (%s provided): %s ", config.Driver.DriverAddr, err)
		}
	}

	log.Infof("UDP connection established. driver: %s. smartball(s): %s", config.Driver.DriverAddr, config.Driver.SmartballsAddr)

	return nil
}

func (d *udpDriver) close() error {

	if d.output == nil {
		log.Warn("Unable to close udp output udp connection. Connection is not set.")
	} else {
		err := d.output.Close()
		if err != nil {
			return fmt.Errorf("unable to close output udp connection: %s ", err)
		}
	}

	if d.input == nil {
		log.Warn("Unable to close udp input udp connection. Connection is not set.")
	} else {
		err := d.input.Close()
		if err != nil {
			return fmt.Errorf("unable to close input udp connection: %s ", err)
		}
	}

	log.Info("UDP connections closed")

	return nil
}

func (d *udpDriver) write(data []byte) error {

	if d.output == nil {
		return fmt.Errorf("unable to write data, udp output connection not set")
	}

	written, err := d.output.Write(data)
	if err != nil {
		return fmt.Errorf("unable to write the data '%s', caused by: %s ", data, err)
	}

	log.Tracef("%d bytes successfully written", written)

	return nil
}

func (d *udpDriver) read(data []byte) error {

	if d.input == nil {
		return fmt.Errorf("unable to read data, udp listening connection not set")
	}

	_, err := d.input.Read(data)
	if err != nil {
		return fmt.Errorf("unable to read the data, caused by: %s ", err)
	}

	return nil
}

// Returns true if the provided ip address is a multicast address
func isMulticast(addr string) bool {
	if len(addr) < 3 {
		return false
	}

	switch addr[:3] {
	case "224", "232", "233", "239":
		return true
	default:
		return false
	}
}

// Parses IMU data from the given payload
func parseIMUdata(data []byte) smartball.ImuData {

	var e error
	offset := 1

	imu := smartball.ImuData{
		Flag: data[0],
	}

	if imu.IsAcc() {
		imu.Acc = parseQ14_2xyz(data[offset : offset+sizeXYZ_Q14_2])
		offset += sizeXYZ_Q14_2
	}

	if imu.IsGyr() {
		imu.Gyr = parseQ14_2xyz(data[offset : offset+sizeXYZ_Q14_2])
		offset += sizeXYZ_Q14_2
	}

	if imu.IsMag() {
		imu.Mag = parseQ14_2xyz(data[offset : offset+sizeXYZ_Q14_2])
		offset += sizeXYZ_Q14_2
	}

	if imu.IsTmp() {
		imu.Tmp, e = smartball.FromFixed(data[offset:offset+sizeQ14_2], q14_2Quotient)
		if e != nil {
			log.Errorf("Unable to parse tmp.")
		}
		offset += sizeQ14_2
	}

	if imu.IsVec() {
		imu.AccN, e = smartball.FromFixed(data[offset:offset+sizeQ14_2], q14_2Quotient)
		if e != nil {
			log.Errorf("Unable to parse Acceleration norm")
		}
		offset += sizeQ14_2

		imu.GyrN, e = smartball.FromFixed(data[offset:offset+sizeQ14_2], q14_2Quotient)
		if e != nil {
			log.Errorf("Unable to parse Gyr norm")
		}
		offset += sizeQ14_2

		imu.MagN, e = smartball.FromFixed(data[offset:offset+sizeQ14_2], q14_2Quotient)
		if e != nil {
			log.Errorf("Unable to parse Mag norm")
		}
		offset += sizeQ14_2
	}

	if imu.IsQua() {
		imu.Qua = parseQuaternion(data[offset : offset+sizeQuaternion])
		offset += sizeQuaternion
	}

	if imu.IsWld() {
		imu.Wld = parseQ14_2xyz(data[offset : offset+sizeXYZ_Q14_2])
	}

	return imu
}

// Parse a number given by the smartball
// size:  size in bytes of the number
// quotient: 10^quotient division on the number
func parseNumber(raw []byte, size int, quotient int) float64 {

	v, e := smartball.FromFixed(raw[0:size], quotient)
	if e != nil {
		log.Errorf("Unable to parse the given value. Max.Float64 value will be applied. Got: %s", raw)
	}

	return v
}

// Parses the "home made format" designed by S.Garnavault to extract XYZ vector components
func parseQ14_2xyz(raw []byte) smartball.Xyz {

	if len(raw) != sizeXYZ_Q14_2 {
		log.Warnf("Invalid parsing X,Y,Z Q14.2 size. got: %s bytes instead of  ", len(raw), sizeXYZ_Q14_2)
	}

	return smartball.Xyz{
		X: parseNumber(raw[0:], sizeQ14_2, q14_2Quotient),
		Y: parseNumber(raw[sizeQ14_2:], sizeQ14_2, q14_2Quotient),
		Z: parseNumber(raw[2*sizeQ14_2:], sizeQ14_2, q14_2Quotient),
	}
}

// Parse a quaternion from the given payload
func parseQuaternion(raw []byte) smartball.Quaternion {

	if len(raw) != sizeQuaternion {
		log.Warnf("Invalid parsing W, X, Y, Z Quaternion size. got: %s bytes instead of  ", len(raw), sizeQuaternion)
	}

	return smartball.Quaternion{
		W: parseNumber(raw[0:], sizeQuaternionComponent, quaternionQuotient),
		X: parseNumber(raw[sizeQuaternionComponent:], sizeQuaternionComponent, quaternionQuotient),
		Y: parseNumber(raw[2*sizeQuaternionComponent:], sizeQuaternionComponent, quaternionQuotient),
		Z: parseNumber(raw[3*sizeQuaternionComponent:], sizeQuaternionComponent, quaternionQuotient),
	}
}
